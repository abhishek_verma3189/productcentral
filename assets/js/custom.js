$(document).ready(function(){
    setSize();
    $(window).resize(setSize);
    $('.row').on('click', function(){
    var link = $(this).find('a').attr('data-target')
    // var link = temp.attr('data-target');
    $('#iframe').attr("src", link);
    });

    function setSize(){
        var height = $(window).height();
        $('iframe').css('height',height+'px');
        $('.scroll').css('max-height',height+'px');
    }

});